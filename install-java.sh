#!/bin/bash

wget https://download.java.net/java/GA/jdk11/28/GPL/openjdk-11+28_linux-x64_bin.tar.gz

tar xzvf openjdk-10.0.2_linux-x64_bin.tar.gz

sudo mv jdk-10.0.2 /usr/lib/jvm/java-10-openjdk-amd64/


# Add the new Java alternative:
sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/java-10-openjdk-amd64/bin/java 1

sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/java-10-openjdk-amd64/bin/javac 1

# Set openjdk-10
sudo update-alternatives --config java

